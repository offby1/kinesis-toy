import logging
import sys

import arrow
import boto3
import click

_STREAM_NAME = "toy-stream"

_log = logging.getLogger(__name__)


def write_one_record(client):
    approximate_write_time = arrow.get()
    data = str(approximate_write_time)

    written_record = client.put_record(StreamName=_STREAM_NAME, Data=str(data).encode("utf-8"), PartitionKey=data[:256])

    _log.info("Wrote %r some time after %s, yielding %s", data, approximate_write_time, dict(written_record, ResponseMetadata='elided'))
    return written_record, approximate_write_time


def get_records(client, fast, timestamp):
    StreamDescription = client.describe_stream(StreamName=_STREAM_NAME)["StreamDescription"]
    for Shard in StreamDescription["Shards"]:
        ShardId = Shard["ShardId"]
        kwargs = dict(StreamName=_STREAM_NAME, ShardId=ShardId)
        if fast:
            kwargs.update(
                ShardIteratorType="AT_TIMESTAMP",
                Timestamp=str(timestamp)
            )
        else:
            kwargs.update(ShardIteratorType="TRIM_HORIZON")

        ShardIterator = client.get_shard_iterator(**kwargs)["ShardIterator"]
        _log.info("Getting records from %r", ShardId)
        while ShardIterator is not None:
            get_records_response = client.get_records(ShardIterator=ShardIterator)
            records = get_records_response["Records"]
            MillisBehindLatest = get_records_response["MillisBehindLatest"]
            _log.info("%s: MillisBehindLatest is %s; records is %s long", ShardId, MillisBehindLatest, len(records))

            for r in records:
                yield (ShardId, r)
            if MillisBehindLatest == 0:
                break
            ShardIterator = get_records_response["NextShardIterator"]
            _log.debug("ShardIterator is now %s", ShardIterator)


@click.command()
@click.option('--fast/--slow', default=True, help='Use AT_TIMESTAMP insead of TRIM_HORIZON')
def main(fast=True):
    found_it = False
    logging.basicConfig(level=logging.INFO)
    client = boto3.client(service_name="kinesis", region_name="us-west-2")
    written_record, approximate_write_time = write_one_record(client)

    for ShardId, r in get_records(client, fast, approximate_write_time):
        Data = r["Data"]
        _log.info("In shard %s, found data %s", ShardId, Data)
        SequenceNumber = r["SequenceNumber"]
        if (ShardId == written_record["ShardId"]) and (SequenceNumber == written_record["SequenceNumber"]):
            _log.info(
                "Woo hoo!  Found what we wrote: ShardId %s, SequenceNumber %s, Data %r", ShardId, SequenceNumber, Data
            )
            found_it = True
            break

    return found_it


if __name__ == "__main__":
    found_it = main.main(sys.argv[1:], standalone_mode=False)

    if found_it:
        sys.exit(0)
    else:
        sys.exit("Couldn't find the thing we stuck into the stream :-(")
