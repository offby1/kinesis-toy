Try to write some data to an (already-exsting) kinesis stream; then read that same data right back out.

The stream exists: arn:aws:kinesis:us-west-2:661326993281:stream/toy-stream

That's my toy AWS account.

Some questions:
- Q: wtf is "TRIM_HORIZON"?  The kinesis docs refer to it, but don't define it

  A: Best I can tell, it means "the dawn of time".

- Q: when do I stop reading from a shard?  When I get bored, or when there's nothing left to read?

  A: When you get bored.

- Q: how do records get removed from a shard?

  A: They don't get explicitly removed; however they expire after however
  long you've configured.  In my case this seems to be 24 hours.

It's not obvious, but I've created a lambda function
(arn:aws:lambda:us-west-2:661326993281:function:doSomethingWithKinesis)
that reads from this stream, and drops the content into Cloudwatch.

Ideally I'd describe this function (and the various ancillary stuff --
IAM roles or whatever) in a cloudformation template that I could get
into actual files in this project.
